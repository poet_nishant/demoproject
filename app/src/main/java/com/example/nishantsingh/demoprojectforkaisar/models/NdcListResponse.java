package com.example.nishantsingh.demoprojectforkaisar.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class NdcListResponse {

    private TSSPJsonResponseForNDCList tsspJsonResponse;

    public TSSPJsonResponseForNDCList getTsspJsonResponse() {
        return tsspJsonResponse;
    }

    public void setTsspJsonResponse(TSSPJsonResponseForNDCList tsspJsonResponse) {
        this.tsspJsonResponse = tsspJsonResponse;
    }
}
