package com.example.nishantsingh.demoprojectforkaisar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by nishant.singh on 11/29/2017.
 */

public class ArrowView extends View {
    private Path ARROW_PATH;
    private Paint paint;

    public ArrowView(Context context) {
        super(context);
    }

    public ArrowView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int x1 = getWidth() / 2;
        int y1 = getHeight() / 2 + 40;
        int x2 = getWidth() / 2 + 200;
        int y2 = getHeight() / 2 + 340;
        Path path = drawCurvedArrow(x1, y1, x2, y2, -150, 4, 2);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setColor(getResources().getColor(R.color.Arrow_color));
        canvas.drawPath(path, paint);
        Paint solidPaint = new Paint();
        solidPaint.setStyle(Paint.Style.FILL);
        solidPaint.setColor(getResources().getColor(R.color.Arrow_color));
        canvas.drawPath(makeArrow(x2, y2), solidPaint);

    }

    public Path drawCurvedArrow(int x1, int y1, int x2, int y2, int curveRadius, int color, int lineWidth) {


        final Path path = new Path();
        int midX = x1 + ((x2 - x1) / 2);
        int midY = y1 + ((y2 - y1) / 2);
        float xDiff = midX - x1;
        float yDiff = midY - y1;
        double angle = (Math.atan2(yDiff, xDiff) * (180 / Math.PI)) - 90;
        double angleRadians = Math.toRadians(angle);
        float pointX = (float) (midX + curveRadius * Math.cos(angleRadians));
        float pointY = (float) (midY + curveRadius * Math.sin(angleRadians));

        path.moveTo(x1, y1);
        path.cubicTo(x1, y1, pointX, pointY, x2, y2);
        return path;
    }

    private static Path makeArrow(float x, float y) {
        Path p = new Path();
        p.moveTo(x, y);
        p.lineTo(x - 10, y + 10);
        p.lineTo(x + 20, y + 20);
        p.lineTo(x + 10, y - 10);
        p.close();
        return p;
    }
}
