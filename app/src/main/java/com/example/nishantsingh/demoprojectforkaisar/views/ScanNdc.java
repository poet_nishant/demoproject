package com.example.nishantsingh.demoprojectforkaisar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

public class ScanNdc extends AppCompatActivity implements View.OnClickListener {

    private static String TAG = ScanNdc.class.getSimpleName();
    private Button btnScanBarCode;
    private Button btnSubmitNumber;
    private Button btnSubmitName;
    private EditText edtNDCName;
    private EditText edtNDCNumber;
    private RelativeLayout relNDCNameContainer;
    private View dividerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_ndc);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.scan_ndc));
        }

        initViewAndListener();

    }

    private void initViewAndListener() {

        btnScanBarCode = (Button) findViewById(R.id.btnScanBarCode);
        btnSubmitNumber = (Button) findViewById(R.id.btnSubmitNumber);
        btnSubmitName = (Button) findViewById(R.id.btnSubmitName);
        edtNDCNumber = (EditText) findViewById(R.id.edtNDCNumber);
        edtNDCName = (EditText) findViewById(R.id.edtNDCName);
        dividerView = (View) findViewById(R.id.divider);
        relNDCNameContainer = (RelativeLayout) findViewById(R.id.relNDCNameContainer);
        btnScanBarCode.setOnClickListener(this);
        btnSubmitNumber.setOnClickListener(this);
        btnSubmitName.setOnClickListener(this);


        edtNDCName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtNDCName.getText().length() > 1) {
                    btnSubmitName.setEnabled(true);
                }
            }
        });

        edtNDCNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                enableSubmitIfReady();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        edtNDCNumber.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                btnScanBarCode.setVisibility(View.GONE);
                dividerView.setVisibility(View.GONE);
                return false;
            }
        });


    }

    private void enableSubmitIfReady() {
        boolean isReady = edtNDCNumber.getText().toString().length() >= 11;
        if (isReady) {
            // check the ndc in arraylist , if found press submit button and go to next screen.
            // if not found display the error message shown in red  and allow user to enter one more field drug name.
            // store it in Intent and pass it to next screen.
            boolean isfoundNdc = scanNDCList(edtNDCNumber.getText().toString());
            if (!isfoundNdc) {
                //edtNDCNumber.setError("NDC not found");
                TextInputLayout til = (TextInputLayout) findViewById(R.id.text_input_layout);
                til.setError("NDC not found");
                relNDCNameContainer.setVisibility(View.VISIBLE);
                btnSubmitNumber.setVisibility(View.INVISIBLE);

            }
        }
        btnSubmitNumber.setEnabled(isReady);
    }

    private boolean scanNDCList(String ndc) {
        JSONObject response = null;
       /* try {
            response = new JSONObject(getString(R.string.jsonresponse));
            JSONArray ndcarray = response.getJSONObject("TSSPJsonResponse").getJSONObject("response").getJSONObject("getPINDCListResponse").getJSONArray("piNDC");
            for (int i = 0; i < ndcarray.length(); i++) {
                Log.d(TAG, ndcarray.getJSONObject(i).get("ndc").toString() + "\n");
            }
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
*/

        if (ndc.equalsIgnoreCase("11111111111")) {
            return true;
        }

        return false;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnSubmitNumber:
                Toast.makeText(this, "IF found the ndc in list , pass it to next screen via intent", Toast.LENGTH_SHORT).show();

                break;
            case R.id.btnScanBarCode:
                Toast.makeText(getApplicationContext(), "Zebra Implementation soon", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btnSubmitName:
                Toast.makeText(this, "Create intent to start next actiity and pass two value", Toast.LENGTH_SHORT).show();
                break;

        }

    }

    private void showDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ScanNdc.this);
        dialog.setCancelable(false);
        dialog.setTitle("Scan NDC");
        dialog.setMessage("Are you sure you want to Scan NDC?");
        dialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
            }
        })
                .setNegativeButton("YES ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

}
