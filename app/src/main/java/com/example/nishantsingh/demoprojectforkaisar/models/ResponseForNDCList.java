package com.example.nishantsingh.demoprojectforkaisar.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class ResponseForNDCList {
    private GetAssignedNDCResponse getAssignedNDCResponse;

    public GetAssignedNDCResponse getGetAssignedNDCResponse() {
        return getAssignedNDCResponse;
    }

    public void setGetAssignedNDCResponse(GetAssignedNDCResponse getAssignedNDCResponse) {
        this.getAssignedNDCResponse = getAssignedNDCResponse;
    }
}

