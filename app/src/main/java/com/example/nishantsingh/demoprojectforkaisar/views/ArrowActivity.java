package com.example.nishantsingh.demoprojectforkaisar;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class ArrowActivity extends AppCompatActivity {

    private ArrowView arrow;
    private Button hide, unhide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arrow);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        arrow = (ArrowView) findViewById(R.id.arrow);
        hide = (Button) findViewById(R.id.hide);
        unhide = (Button) findViewById(R.id.unhide);
        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrow.setVisibility(View.GONE);
            }
        });
        unhide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrow.setVisibility(View.VISIBLE);
            }
        });



/*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

}
