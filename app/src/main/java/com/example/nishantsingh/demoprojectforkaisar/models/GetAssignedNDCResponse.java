package com.example.nishantsingh.demoprojectforkaisar.models;

import java.util.ArrayList;

/**
 * Created by is-4586 on 11/2/17.
 */

public class GetAssignedNDCResponse {

    private String phmNo;
    private ArrayList<NDC> Ndc;
    private VarianceNDC varianceNDC;

    public String getPhmNo() {
        return phmNo;
    }

    public void setPhmNo(String phmNo) {
        this.phmNo = phmNo;
    }

    public ArrayList<NDC> getNdc() {
        return Ndc;
    }

    public NDC getNdc(int i) { return Ndc.get(i);}

    public void setNdc(ArrayList<NDC> ndc) {
        Ndc = ndc;
    }

    public VarianceNDC getVarianceNDC() {
        return varianceNDC;
    }

    public void setVarianceNDC(VarianceNDC varianceNDC) {
        this.varianceNDC = varianceNDC;
    }
}
