package com.example.nishantsingh.demoprojectforkaisar.models;

/**
 * Created by is-4586 on 11/2/17.
 */

public class RecountHistory {

    private String counternuid;
    private String varianceQty;
    private Object countername;
    private String date;
    private double variancetotalcost;
    private double varianceunitcost;

    public String getVarianceQty() {
        return varianceQty;
    }

    public void setVarianceQty(String varianceQty) {
        this.varianceQty = varianceQty;
    }

    public String getCounternuid() {
        return counternuid;
    }

    public void setCounternuid(String counternuid) {
        this.counternuid = counternuid;
    }

    public Object getCountername() {
        return countername;
    }

    public void setCountername(Object countername) {
        this.countername = countername;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getVariancetotalcost() {
        return variancetotalcost;
    }

    public void setVarianceunitcost(double varianceunitcost) {
        this.varianceunitcost = varianceunitcost;
    }

    public double getVarianceunitcost() {
        return varianceunitcost;
    }

    public void setVariancetotalcost(double variancetotalcost) {
        this.variancetotalcost = variancetotalcost;
    }

}
