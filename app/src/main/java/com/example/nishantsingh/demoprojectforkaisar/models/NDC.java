package com.example.nishantsingh.demoprojectforkaisar.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.List;

/**
 * Created by is-4586 on 11/2/17.
 */

@Entity(tableName = "ndc")
public class NDC {

    @PrimaryKey(autoGenerate = true)
    private String barcode;

    @ColumnInfo(name = "counts")
    private long counts;

    @ColumnInfo(name = "packs_per_container")
    private int packsPerContainer;

    @ColumnInfo(name = "assign_date")
    private String assignDate;

    @ColumnInfo(name = "approval_due_time")
    private Object approvalDueTime;

    @ColumnInfo(name = "assigned_date_overdue")
    private String assignedDateOverdue;

    @ColumnInfo(name = "count_exhausted")
    private Object countExhausted;

    @ColumnInfo(name = "ccid")
    private int ccid;

    @ColumnInfo(name = "counted_date")
    private Object countedDate;

    @ColumnInfo(name = "last_counted_timestamp")
    private Object lastCountedTimestamp;

    @ColumnInfo(name = "eps_on_hand_qty")
    private long epsOnHandQty;

    @ColumnInfo(name = "drug_name")
    private String drugName;

    @ColumnInfo(name = "gpi")
    private String gpi;

    @ColumnInfo(name = "ndc")
    private String ndc;

    @ColumnInfo(name = "manufacturer")
    private String manufacturer;

    @ColumnInfo(name = "pack")
    private int pack;

    @ColumnInfo(name = "time_out")
    private Object timeOut;

    @ColumnInfo(name = "recount_history")
    private List<RecountHistory> recounthistory;

    @ColumnInfo(name = "recount_due_time")
    private Object recountDueTime;

    @ColumnInfo(name = "schedule")
    private String schedule;

    @ColumnInfo(name = "variance_qty")
    private String varianceQty;

    @ColumnInfo(name = "uom")
    private String uom;

    @ColumnInfo(name = "variance_total_cost")
    private long varianceTotalCost;

    @ColumnInfo(name = "variance_unit_cost")
    private long varianceUnitCost;

    public long getCounts() {
        return counts;
    }

    public void setCounts(long counts) {
        this.counts = counts;
    }

    public int getPacksPerContainer() {
        return packsPerContainer;
    }

    public void setPacksPerContainer(int packsPerContainer) {
        this.packsPerContainer = packsPerContainer;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getAssignDate() {
        return assignDate;
    }

    public void setAssignDate(String assignDate) {
        this.assignDate = assignDate;
    }

    public Object getApprovalDueTime() {
        return approvalDueTime;
    }

    public void setApprovalDueTime(Object approvalDueTime) {
        this.approvalDueTime = approvalDueTime;
    }

    public String getAssignedDateOverdue() {
        return assignedDateOverdue;
    }

    public void setAssignedDateOverdue(String assignedDateOverdue) {
        this.assignedDateOverdue = assignedDateOverdue;
    }

    public Object getCountExhausted() {
        return countExhausted;
    }

    public void setCountExhausted(Object countExhausted) {
        this.countExhausted = countExhausted;
    }

    public int getCcid() {
        return ccid;
    }

    public void setCcid(int ccid) {
        this.ccid = ccid;
    }

    public Object getCountedDate() {
        return countedDate;
    }

    public void setCountedDate(Object countedDate) {
        this.countedDate = countedDate;
    }

    public Object getLastCountedTimestamp() {
        return lastCountedTimestamp;
    }

    public void setLastCountedTimestamp(Object lastCountedTimestamp) {
        this.lastCountedTimestamp = lastCountedTimestamp;
    }

    public long getEpsOnHandQty() {
        return epsOnHandQty;
    }

    public void setEpsOnHandQty(long epsOnHandQty) {
        this.epsOnHandQty = epsOnHandQty;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getGpi() {
        return gpi;
    }

    public void setGpi(String gpi) {
        this.gpi = gpi;
    }

    public String getNdc() {
        return ndc;
    }

    public void setNdc(String ndc) {
        this.ndc = ndc;
    }

    public int getPack() {
        return pack;
    }

    public void setPack(int pack) {
        this.pack = pack;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Object getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Object timeOut) {
        this.timeOut = timeOut;
    }

    public List<RecountHistory> getRecounthistory() {
        return recounthistory;
    }

    public void setRecounthistory(List<RecountHistory> recounthistory) {
        this.recounthistory = recounthistory;
    }

    public Object getRecountDueTime() {
        return recountDueTime;
    }

    public void setRecountDueTime(Object recountDueTime) {
        this.recountDueTime = recountDueTime;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getVarianceQty() {
        return varianceQty;
    }

    public void setVarianceQty(String varianceQty) {
        this.varianceQty = varianceQty;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public long getVarianceTotalCost() {
        return varianceTotalCost;
    }

    public void setVarianceTotalCost(long varianceTotalCost) {
        this.varianceTotalCost = varianceTotalCost;
    }

    public long getVarianceUnitCost() {
        return varianceUnitCost;
    }

    public void setVarianceUnitCost(long varianceUnitCost) {
        this.varianceUnitCost = varianceUnitCost;
    }
}
