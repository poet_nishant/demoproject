package com.example.nishantsingh.demoprojectforkaisar.core;

import com.example.nishantsingh.demoprojectforkaisar.models.NdcListResponse;
import com.google.gson.Gson;

/**
 * Created by is-4586 on 11/6/17.
 */

public class JsonParser {

    public static NdcListResponse ParseJsonForNdcList(String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, NdcListResponse.class);
    }


}
