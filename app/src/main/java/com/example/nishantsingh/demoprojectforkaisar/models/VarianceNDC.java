package com.example.nishantsingh.demoprojectforkaisar.models;

import java.util.ArrayList;

/**
 * Created by is-4586 on 11/2/17.
 */

public class VarianceNDC {

    private ArrayList<NDC> Ndc;
    private String phmNo;

    public ArrayList<NDC> getNdc() {
        return Ndc;
    }

    public void setNdc(ArrayList<NDC> ndc) {
        Ndc = ndc;
    }

    public String getPhmNo() {
        return phmNo;
    }

    public void setPhmNo(String phmNo) {
        this.phmNo = phmNo;
    }
}
